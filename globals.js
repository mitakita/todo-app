const TODO_LIST_KEY = 'todos';

function get(id) {
  return $(`#${id}`);
}

function hideElement(id) {
  get(id).addClass('not-visible');
}

function showElement(id) {
  get(id).removeClass('not-visible');
}

function removeTodosFromLocalStorage() {
  localStorage.removeItem(TODO_LIST_KEY);
}

function getTodosFromLocalStorage() {
  let todoItemList = localStorage.getItem(TODO_LIST_KEY);
  return todoItemList ? JSON.parse(todoItemList) : [];
}

function getTodoItem(todoId) {
  return $.grep(getTodosFromLocalStorage(), (item) => { return item.id === todoId })[0];  
}

function saveToLocalStorage(todos) {
  return $.Deferred((defer) => {
    try {
      localStorage.setItem(TODO_LIST_KEY, JSON.stringify(todos));
      defer.resolve();
    } catch (error) {
      defer.reject(error);
    }
  });
}

function getIndexFromId(id) {
  let todoList = getTodosFromLocalStorage();
  return todoList.findIndex((element) => {
    return element.id === id;
  });
}

function removeFromLocalStorage(index) {
  return $.Deferred((defer) => {
    try {
      let todos = getTodosFromLocalStorage();
      todos.splice(index, 1);
      localStorage.setItem(TODO_LIST_KEY, JSON.stringify(todos));
      defer.resolve();
    } catch (error) {
      defer.reject(error);
    }
  });
}
