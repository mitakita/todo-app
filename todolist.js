const TODO_LIST_NAME = 'todoList';
const ADD_BUTTON_NAME = 'addButton';
const EDIT_BUTTON_NAME = 'editButton';
const CLEAR_BUTTON_NAME = 'clearButton';
const CANCEL_BUTTON_NAME = 'cancelButton';

const DESCRIPTION_NAME = 'todoDescription';
const ESTIMATE_NAME = 'todoEstimate';
const TIME_SPENT_NAME = 'todoTimeSpent';

const TIME_SPENT_GROUP_NAME = 'timeSpentFormGroup';

const DESCRIPTION_ERROR_NAME = 'todoDescriptionError';
const ESTIMATE_ERROR_NAME = 'todoEstimateError';
const TIME_SPENT_ERROR_NAME = 'todoTimeSpentError';

let currentTodoItemId;

function init() {
  buildTodoList();
}

function clearAll() {
  if (confirm('Are you sure you want to delete all todo items?')) {
    removeTodosFromLocalStorage();
    get(TODO_LIST_NAME).html('');
    hideDonut();
  }
}

function buildTodoList() {
  if (getTodosFromLocalStorage()) {
    createTodoList();
  }
}

function createTodoList() {
  get(TODO_LIST_NAME).html(getTodoListHtml());
}

function getTodoListHtml() {
  let todos = getTodosFromLocalStorage();
  let innerHtml = '<ul class="list-group">';

  for (let i = 0; i < todos.length; i++) {
    innerHtml += buildListItem(todos[i]);
  }

  innerHtml += '</ul>'
  return innerHtml;
}

function buildListItem(todo) {
  return `<li id="${todo.id}" class="list-group-item justify-content-between todo-list-item"
              onclick="handleListItemClick('${todo.id}')"
              ondrop="drop(event)"
              ondragover="allowDrop(event)"
              draggable="true"
              ondragstart="drag(event)">
            ${todo.description}
            <span>
              <span class="${getBadgeClasses(todo)}"
                    title="Time to complete / Time spent">
                ${todo.estimate} / ${todo.timeSpent}
              </span>
              <span class="badge badge-remove"
                    title="Remove item"
                    onclick="handleRemove('${todo.id}')">x</span>
            </span>
          </li>`
}

function getBadgeClasses(todo) {
  if (todo.estimate > todo.timeSpent) {
    return 'badge badge-estimate';
  }
  return 'badge badge-done'
}

function handleListItemClick(todoId) {
  clearErrors();
  indicateListItemSelected(todoId);

  let updateItem = getTodoItem(todoId);
  
  if (!updateItem) return;

  setEditingState(updateItem);
  showDonut(updateItem);
}

function indicateListItemSelected(id) {
  get(currentTodoItemId).removeClass('active');
  get(id).addClass('active');
  currentTodoItemId = id;
}

function setEditingState(updateItem) {
  setValue(DESCRIPTION_NAME, updateItem.description);
  setValue(ESTIMATE_NAME, updateItem.estimate);
  setValue(TIME_SPENT_NAME, updateItem.timeSpent);

  hideElement(ADD_BUTTON_NAME);
  hideElement(CLEAR_BUTTON_NAME);
  showElement(EDIT_BUTTON_NAME);
  showElement(CANCEL_BUTTON_NAME);
}

function cancelEditTodoItem() {
  setInitialState();
}

function setInitialState() {
  setValue(DESCRIPTION_NAME, '');
  setValue(ESTIMATE_NAME, '');
  setValue(TIME_SPENT_NAME, '');

  hideElement(EDIT_BUTTON_NAME);
  hideElement(CANCEL_BUTTON_NAME);
  showElement(ADD_BUTTON_NAME);
  showElement(CLEAR_BUTTON_NAME);
}

function handleRemove(id) {
  removeFromLocalStorage(getIndexFromId(id));
  init();
}

function saveNewTodoItem() {
  if (!isValid()) {
    return;
  }

  let todos = getTodosFromLocalStorage();
  todos.push(getNewTodo());
  saveToLocalStorage(todos);

  setInitialState();
  init();
}

function saveEditedTodoItem() {
  if (!isValid()) {
    return;
  }

  let todos = editTodoItem();
  saveToLocalStorage(todos);

  setInitialState();

  updateDonut();
  init();
}

function editTodoItem() {
  let todos = getTodosFromLocalStorage();
  let index = getIndexFromId(currentTodoItemId);
  todos[index] = getCurrentTodoValue(currentTodoItemId);
  return todos;
}

function isValid() {
  let isOk = true;
  
  isOk &= checkElement(DESCRIPTION_NAME, DESCRIPTION_ERROR_NAME);
  isOk &= checkElement(ESTIMATE_NAME, ESTIMATE_ERROR_NAME);
  isOk &= checkElement(TIME_SPENT_NAME, TIME_SPENT_ERROR_NAME);

  return isOk;
}

function checkElement(elementName, errorName) {
  let isOk = getValue(elementName) ? true : false;

  if (!isOk) {
    showElement(errorName);
  } else {
    hideElement(errorName);
  }
  
  return isOk;
}

function clearErrors() {
  hideElement(DESCRIPTION_ERROR_NAME);
  hideElement(ESTIMATE_ERROR_NAME);
  hideElement(TIME_SPENT_ERROR_NAME);
}

function getNewTodo() {
  return getCurrentTodoValue(getNewId());
}

function getNewId() {
  return chance.guid();
}

function getCurrentTodoValue(id) {
  return {
    id: id,
    description: getValue(DESCRIPTION_NAME),
    estimate: Number(getValue(ESTIMATE_NAME)),
    timeSpent: Number(getValue(TIME_SPENT_NAME))
  };
}

function getValue(id) {
  return get(id).val();
}

function setValue(id, value) {
  get(id).val(value);
}

function loadDummyData() {
  let todos = getTodosFromLocalStorage();
  todos.push.apply(todos, getDummyData());
  saveToLocalStorage(todos);
  init();
}

function getDummyData() {
  return [
    {
      id: getNewId(),
      description: 'Write a book',
      estimate: 200,
      timeSpent: 30
    },
    {
      id: getNewId(),
      description: 'Build a house',
      estimate: 300,
      timeSpent: 40
    },
    {
      id: getNewId(),
      description: 'Drive across the country',
      estimate: 80,
      timeSpent: 30
    },
    {
      id: getNewId(),
      description: 'Watch a movie',
      estimate: 1.5,
      timeSpent: 0.5
    },
    {
      id: getNewId(),
      description: 'Do the dishes',
      estimate: 0.5,
      timeSpent: 0
    }
  ];
}
