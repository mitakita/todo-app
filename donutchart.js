const DONUT_CHART_WRAPPER_NAME = 'donutChartWrapper';
const DONUT_TITLE_NAME = 'donutTitle';
const DONUT_TEXT_NAME = 'donutText';
const DONUT_SEGMENT_NAME = 'donutSegment';

const CIRCUMFERENCE = 50 * 2 * Math.PI;
const STROKE_DASH_OFFSET = CIRCUMFERENCE * 0.5;

let currentTodoItem;

function hideDonut() {
  hideElement(DONUT_CHART_WRAPPER_NAME);
}

function updateDonut() {
  if (currentTodoItem) {
    showDonut(getTodoItem(currentTodoItem.id));
  }
}

function showDonut(todoItem) {
  currentTodoItem = todoItem;
  get(DONUT_TITLE_NAME).html(`Progress for: ${todoItem.description}`);
  showElement(DONUT_CHART_WRAPPER_NAME);

  let donutText = get(DONUT_TEXT_NAME).html(getDonutText(todoItem));

  let donutSegment = get(DONUT_SEGMENT_NAME);
  donutSegment.css({
    'stroke-dasharray': getStrokeDashArray(todoItem),
    'stroke-dashoffset': STROKE_DASH_OFFSET
  })
  showElement(DONUT_SEGMENT_NAME);
}

function getDonutText(todoItem) {
  return `${getPercentageValue(todoItem)}%`;
}

function getPercentageValue(todoItem) {
  let rawValue = Math.round(todoItem.timeSpent / todoItem.estimate * 100);
  return (rawValue > 100) ? 100 : rawValue;
}

function getStrokeDashArray(todoItem) {
  let totalTime = todoItem.estimate;
  let spentTime = getTimeSpent(todoItem);
  let timeLeft = totalTime - spentTime;

  let spentTimePercentage = spentTime / totalTime;
  let timeLeftPercentage = timeLeft / totalTime;

  let spentTimeStrokeSize = CIRCUMFERENCE * spentTimePercentage;
  let restStrokeSize = CIRCUMFERENCE - spentTimeStrokeSize;

  return `${spentTimeStrokeSize} ${restStrokeSize}`;
}

function getTimeSpent(todoItem) {
  if (todoItem.timeSpent > todoItem.estimate) {
    return todoItem.estimate;
  }
  return todoItem.timeSpent ? todoItem.timeSpent : 0;
}
