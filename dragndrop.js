const DATA_ID = 'todoItem';

function allowDrop(event) {
  event.preventDefault();
}

function drag(event) {
  event.dataTransfer.setData(DATA_ID, event.target.id);
}

function drop(event) {
  let above = event.layerY < (event.target.offsetHeight / 2);
  let sourceId = event.dataTransfer.getData(DATA_ID);
  move(sourceId, event.target.id, above);
}

function move(sourceId, targetId, before) {
  let movedItem = getTodoItem(sourceId);

  removeFromLocalStorage(getIndexFromId(sourceId)).then(() => {
    insertMovedItem(movedItem, getIndexFromId(targetId), before).then(() => {
      init();
    })
  });
}

function insertMovedItem(movedItem, targetIndex, before) {
  let todos = getTodosFromLocalStorage();
  let startIndex = before ? targetIndex : targetIndex + 1;
  todos.splice(startIndex, 0, movedItem);
  return saveToLocalStorage(todos);
}